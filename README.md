# Assessment_3-WDGET2024067


### Tech Stack 

- Node 
    - express
    - eslint
- Gitlab 
- Jenkins (with required plugins gitlab,git)
- ngrok

Test the Gitlab connection from Jenkins before starting.
Ensure that personal access token has been created in gitlab and configured in jenkins credentials.

### Stages

- Initializing repository
- Checking out to new branch **master**
- Pushing the code files
- Tunnelling IP (ngrok)
- Configuring webhook
- Setting up gitlab pipeline & Jenkinsfile


## Tunneling IP

Since webhook requires a public IP, and jenkins runs in the local machine, you can change the domain for jenkins, if you have one, else
  - Install ngrok 
  - Sign up and get the auth token, run it in the terminal
  - run bash ``` ngrok http http://localhost:8080 ``` in terminal
  This will provide you a public domain, we will use this to create webhook


## Jenkins - reconfigure

- Navigate to Manage jenkins -> System -> Jenkins location.
- Change the jenkins URL to the public domain from ngrok.
- Create a pipeline.
- Under Pipeline -> Definition -> Choose Pipeline script from SCM.
- Choose SCM as Git, paste the gitlab repository URL .
- Choose the credentials.
- Specify */master as branch.
- Uncheck Lightweight checkout and click Save.


## Webhook Configuration
- **Jenkins**
  - Under Build triggers -> Enable Build when a change is pushed to GitLab.
  - Under Advanced, generate the secret token.
- **GitLab**
  - Navigate to repository settings -> webhook -> Add new webhook.
  - Paste the URL and Secret token from jenkins.
  - Enable push events.
  - Test the webhook, it should return ```HTTP 200``` for a successful test.

## Gitlab
- Create **.gitlab-ci.yml** in the master branch, containing a script to trigger the jenkins pipeline.
- --url points to the ngrok URL that points to the Jenkins job for a project named "node-app."
- --header "Content-Type: application/json": Specifies the content type of the request as JSON.
- --data "{\"source\":\"GitLab\",\"jobName\":\"node-app\",\"secret\":\"SECRET_TOKEN\"}"': The JSON payload that will be sent in the POST request. It includes information about the source (GitLab), the job name ("node-app"), and a secret key.

## Jenkinsfile
- Create a file named Jenkinsfile and add to the master branch.
- Code the pipeline script in the file as required.

## Jenkins Trigger from Local machine
![Screenshot](https://gitlab.com/group16972007/assessment-3_wdget2024067/-/raw/master/images/screenshor.png)

